﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Hover physics mover.
/// <para>This mover keeps the controlled object hovering above ground</para>
/// </summary>
public class HoverPhysicsMover : PhisicesMover  
{

	#region Public Members
	/// <summary>
	/// The height of the hover obove the ground.
	/// </summary>
	public float hoverHeight;
	/// <summary>
	/// The height tollerance.
	/// </summary>
	public float hoverHeightTollerance;
	/// <summary>
	/// The hover speed tollerance.
	/// </summary>
	public float hoverSpeedTollerance;
	/// <summary>
	/// The maximum force that will be used to maintain height.
	/// <para>this is in addition to gravity</para>
	/// </summary>
	public float maxUpThrust;
	/// <summary>
	/// The minimum up thrust.
	/// <para>this is in subtraction to gravity so a value of one means one unit under gravity</para>
	/// </summary>
	public float minUpThrust;



	/// <summary>
	/// The gap radius around the target height that will be used to calculate the ratio
	/// of the force used to achive height target.
	/// </summary>
	public float hoverGap;
	#region Jumping
	/// <summary>
	/// The jump meter.
	/// <para>THe time in seconds the object can jump</para>
	/// </summary>
	public float jumpMeter;
	/// <summary>
	/// The target height of a jump
	/// </summary>
	public float jumpHeight;
	/// <summary>
	/// The jump meter recovery rate.
	/// </summary>
	public float jumpMeterRecoveryRate;
	#endregion
	#endregion

	#region Private Members
	/// <summary>
	/// when true vertical force will always counter gravity.
	/// </summary>
	private bool forceAntiGrav;
	[SerializeField]
	private float jumpTimmer;


	/// <summary>
	/// a flag indicationg a jump was just initiated
	/// </summary>
	private bool justJumped;

	private float lastDtg=-1f;


	#endregion

	void Start() 
	{
		init ();
	}

	protected override void init ()
	{
		base.init ();
		jumpTimmer = jumpMeter;
	}

	public override void getCommands (CommandMask commandMask)
	{
		bool normalHover = true;
		float cdgt = getDistanceToGround ();
		horizontalKey=false;
//		Debug.Log (cdgt);
		if (commandMask.containsCommand (UserCommands.FIXED_UPDATE_CALL)) {

			if (cdgt == -1f) {
				if (!commandMask.containsCommand (UserCommands.JUMP)) {
					lastDtg = -1f;
				}
			}
			else 
			{
				lastDtg=cdgt;
			}
		} 


		if(commandMask.containsCommand(UserCommands.RIGHT_THRUST))
		{
			horizontalKey=true;
			if(facingRight)
			{
				
				_rbody.AddForce(Vector3.right*forwardForce);
				
			}
			else
			{
				_rbody.AddForce(Vector3.right*breakForce);
				if(Mathf.Abs(_rbody.velocity.x)<turnTollarance)
				{
					turnAround();
				}
				
			}
		
		}
		else if(commandMask.containsCommand(UserCommands.LEFT_THRUST))
		{
			horizontalKey=true;
			if(facingRight)
			{
				_rbody.AddForce(Vector3.left*breakForce);
				if(Mathf.Abs(_rbody.velocity.x)<turnTollarance)
				{
					turnAround();
				}
			}
			else
			{
				_rbody.AddForce(Vector3.left*forwardForce);
			}
		}
		if(commandMask.containsCommand(UserCommands.JUMP))
		{
			float dtg = getDistanceToGround();

			if(jumpTimmer>0f)
			{
				if(dtg==-1f&&lastDtg==-1f)
				{
					forceAntiGrav = true;
					_rbody.velocity = new Vector3(_rbody.velocity.x,0,0);
				}
				maintainHeight(jumpHeight,groundSensor.position.y - lastDtg );
				normalHover=false;
				jumpTimmer-=Time.fixedDeltaTime;
			}
		}
		if (normalHover) 
		{
			//Debug.Log("keep");
			maintainHeight(  hoverHeight,groundSensor.position.y - lastDtg);
			if(jumpTimmer<jumpMeter && cdgt<=(hoverHeight+hoverHeightTollerance*2f)&&cdgt>0)
			{
				jumpTimmer+=jumpMeterRecoveryRate;
				if(jumpTimmer>jumpMeter)
				{
					jumpTimmer=jumpMeter;
				}
			}
		}
	}

	void FixedUpdate()
	{



		decayVertical ();

		limitVelocity ();
		if ((decayWhenPressed)||(!horizontalKey)) 
		{
			//Debug.Log("decay");
			decayHorizontal ();



		}
		haltHorizontal ();
	}


	/// <summary>
	/// Maintains the target height.
	/// <para>adds force up agains gravity</para>
	/// </summary>
	private void maintainHeight(float targetHeight,float floorY)
	{

		if (forceAntiGrav) {
			_rbody.AddForce (-getGravityForce ());
			forceAntiGrav=false;
		} 
		else 
		{
			if(floorY>groundSensor.position.y)
			{
				_rbody.AddForce(Vector3.up*minUpThrust);
				return;
			}
			float dtg =Mathf.Abs( groundSensor.position.y-floorY);
			if (Mathf.Abs ((groundSensor.position.y-(floorY + targetHeight))) < hoverHeightTollerance &&
				Mathf.Abs (_rbody.velocity.y) < hoverSpeedTollerance) {
				_rbody.velocity = new Vector3 (_rbody.velocity.x, 0, 0);
				_rbody.gameObject.transform.position = new Vector3 (_rbody.gameObject.transform.position.x,
				                                                    targetHeight+floorY
				                                                    , 0);
				//Debug.Log ("halt");
			} else {
				float upForce = getGravityForce().magnitude;//base gravity force.

				float ratio=0f;
				if(dtg==-1)
				{
					upForce=minUpThrust;
				}
				else if(dtg>targetHeight+hoverGap)
				{
					ratio=-1;
				}else if(dtg<targetHeight-hoverGap)
				{
					ratio=1;
				}
				else
				{
					ratio =Mathf.Round( -(dtg-targetHeight)/hoverGap*10)/10f;
				}

				//Debug.Log (getDistanceToGround ().ToString ());
				//Debug.Log ("ratio: "+ratio);
				if (ratio > 0f) {
					upForce += Mathf.Abs(upForce - maxUpThrust )*ratio;
				} else if (ratio < 0f) {
					upForce += Mathf.Abs(upForce - minUpThrust) *ratio;
				}
				//Debug.Log("f:"+upForce);
				_rbody.AddForce (Vector3.up * upForce);

			}
		}
	}



}
